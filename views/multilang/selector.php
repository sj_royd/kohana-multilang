<div id="multilang">
	<p class="multilang-title"><?php echo Multilang::currentLanguage($langKey) ?></p>
	<ul class="multilang-selector">
	<?php foreach($selectors as $selector): ?>
		<li><?php echo $selector; ?></li>
	<?php endforeach; ?>
	</ul>
</div>

