<?php


class Multilang_HTML extends Kohana_HTML {

	/**
	 *
	 * @param string $uri
	 * @param type $title
	 * @param array $attributes
	 * @param type $protocol
	 * @param type $index
	 * @return type
	 * @see HTML::anchor
	 */
	public static function lang_anchor($uri, $title = NULL, array $attributes = NULL, $protocol = NULL, $index = TRUE) {
		$conf = Kohana::$config->load('multilang');
		if($conf && $conf->get('strategy') == 'url'){
			$uri = I18n::lang().'/'.$uri;
		}
		return parent::anchor($uri, $title, $attributes, $protocol, $index);
	}

}
