<?php

/**
 *
 * @author Kamil Boczek
 */
class Multilang_Request extends Kohana_Request {

	public function execute(){
		Multilang::init();
		return parent::execute();
	}

}
