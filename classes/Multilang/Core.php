<?php

/**
 *
 * @author Kamil Boczek
 */
class Multilang_Core {

	public static function init(){

		$config = Kohana::$config->load('multilang');

		// Get the list of supported languages
		$langs = $config->languages;

		$lang = Cookie::get('lang', $config->default);
		if(($queryLang = Request::initial()->query('set-lang'))){
			$lang = $queryLang;
			Cookie::set('lang', $lang, $config->cookieLifeTime);
		}
		if(!array_key_exists($lang, $config->languages)){
			// Check the allowed languages, and force the default
			$lang = $config->default;
			Cookie::set('lang', $lang, $config->cookieLifeTime);
		}

		if(isset($langs[$lang])){
			// Set the target language
			I18n::lang($langs[$lang]['i18n']);

			// Set locale
			setlocale(LC_ALL, $langs[$lang]['locale']);
		}

	}

	/**
	 * Return a language simple associative array
	 * @return array
	 */
	public static function asSimpleArray(){
		$output = [];
		foreach(Kohana::$config->load('multilang')->get('languages') as $lang => $settings){
			$output[$lang] = $settings['label'];
		}
		return $output;
	}

	/**
	 * Return a language selector menu
	 * @return View
	 */
	public static function selector($key = 'label', $langIcon = false){
		$htmlSelectors = [];
		$selectors = [];
		$config = Kohana::$config->load('multilang');
		// Create uris for each language
		foreach($config->get('languages') as $lang => $language){
			switch ($config->get('strategy')){
				case 'cookie':
					$url = '?set-lang='.$language['i18n'];
					break;
				case 'url':
					$url = $language['i18n'];
					break;
			}
			$selectors[$lang] = [
				'url' => $url,
				'label' => $language[$key]
			];
			// If it's the current language
			if($language['i18n'] === I18n::lang()){
				$selectors[$lang]['url'] = null;
				$htmlSelectors[$lang] = "<span class=\"multilang-selected multilang-{$lang}\">{$language[$key]}</span>";
			} else {
				$htmlSelectors[$lang] = HTML::anchor(
						$url,
						$language[$key],
						['class' => "multilang-selectable multilang-{$lang}", 'title' => $language[$key]]
				);
			}
		}

		// We display the menu only if we can select another language for this page
		if(count($selectors) > 1){
			return View::factory('multilang/selector')
				->set('langKey', $key)
				->set('currentLang', Multilang::currentLanguage())
				->bind('htmlSelectors', $htmlSelectors)
				->bind('selectors', $selectors);
		}
		return '';
	}


	public static function currentLanguage($key = null){
		$config = Kohana::$config->load('multilang');
		$language = $config->languages[I18n::lang()];
		if($key === null){
			return $language;
		} else {
			return $language[$key];
		}
	}

}
